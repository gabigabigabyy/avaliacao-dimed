package com.gabrielasantos.dimedavaliacao.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.gabrielasantos.dimedavaliacao.model.Bus;

@RestController
@RequestMapping(value = "/onibus")
public class busController {

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/linhas")
	public List<Bus> listaOnibus() {
		String url = "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o";
		Bus[] arrayOnibus = restTemplate.getForObject(url, Bus[].class);

		return Arrays.asList(arrayOnibus);
	}

	@GetMapping("/{nome}")
	public Bus filtrarOnibus(@PathVariable String nome) {
		List<Bus> todosOnibus = listaOnibus();

		for (Bus onibus : todosOnibus) {
			if (onibus.getNome().equals(nome)) {

				return onibus;
			}

		}
		return null;

	}

}
